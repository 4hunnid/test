import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid')
    userId: string;

    @Column()
    joinCount: number;

    @Column()
    url: string;

    @Column()
    shortUrl: string;
}
