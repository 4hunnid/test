export const isUrl = (url: string): boolean => /^(https?):\/\/[^\s$.?#].[^\s]*$/gm.test(url);

export const generateUrl = (max: number): string => Math.random().toString(36).substring(max);