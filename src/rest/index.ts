import express from 'express';
import cors from 'cors';
import { createConnection, getRepository, Repository } from 'typeorm';
import { User } from './Entities/User.entity';
import { generateUrl, isUrl } from './Utils/util';

export const app = express();
const PORT = 3000;
const URL = `http://localhost:${PORT}`;

let userRep: Repository<User>;

app.listen(PORT, async () => {
    console.log(`Server start on port ${PORT}`);

    await createConnection();
    userRep = getRepository(User);
});

app.use(cors());

app.post('/addurl', async (req, res) => {
    const url = String(req.params.url);

    if (url && isUrl(url)) {
        const user = new User();
        const newUrl = generateUrl(7);

        user.joinCount = 0;
        user.userId = newUrl;
        user.url = url;

        await userRep.save(user);

        res.json({ success: true, url: `${URL}/${newUrl}` });
    } else {
        res.json({ success: false });
    }
});

app.get('/:url', async (req, res) => {
    const user = await userRep.findOne({ where: { userId: req.params.url } });

    if (user?.userId) {
        res.redirect(user.url);

        user.joinCount++;
        await userRep.save(user);
    } else {
        res.status(404);
    }
});
