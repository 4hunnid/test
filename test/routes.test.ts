import { app } from '../src/rest';
import request from 'supertest';

describe('Test the root path', () => {
    // test('database connect', () => {});

    test('regex addurl check', () => {
        return request(app)
            .post('/addurl')
            .set({ url: 'dadadtest' })
            .then(response => {
                expect(response.body.success).toBe(false);
            });
    });

    test('addurl success check', () => {
        return request(app)
            .post('/addurl')
            .set({ url: 'https://google.com' })
            .then(res => {
                expect(res.body.success).toBe(true);
            });
    });
});
